﻿using UnityEngine;
using System.Collections;

public class CharacterController : MonoBehaviour {
    public Quaternion QFacingDirection;
    public string  MovingDirection;
    public bool    isSprinting;

    public Rigidbody rb;
    public Transform bottomTr;
    public Transform topTr;
    public Transform tr;

    public CharacterStats statsReference;   

    void Start () {
        rb = GetComponent<Rigidbody>();
        tr = GetComponent<Transform>();
        statsReference = GetComponent<CharacterStats>();
	}
	
	void Update () {
        { //directioning
            float speed;
            speed = statsReference.speed;
            if (isSprinting == true) speed = 1.5f * speed;
            if (MovingDirection == null || MovingDirection == "STOP")
                rb.velocity = new Vector3(0, 0, 0);
            if (MovingDirection == "N")
            {
                rb.velocity = new Vector3(0, 0, speed);
                bottomTr.eulerAngles = new Vector3(0, 0, 0);
            }
            if (MovingDirection == "S")
            {
                rb.velocity = new Vector3(0, 0, -speed);
                bottomTr.eulerAngles = new Vector3(0, 180, 0);
            }
            if (MovingDirection == "E")
            {
                rb.velocity = new Vector3(speed, 0, 0);
                bottomTr.eulerAngles = new Vector3(0, 90, 0);
            }
            if (MovingDirection == "W")
            {
                rb.velocity = new Vector3(-speed, 0, 0);
                bottomTr.eulerAngles = new Vector3(0, 270, 0);
            }
            if (MovingDirection == "NE")
            {
                rb.velocity = new Vector3(speed, 0, speed);
                bottomTr.eulerAngles = new Vector3(0, 45, 0);
            }
            if (MovingDirection == "NW")
            {
                rb.velocity = new Vector3(-speed, 0, speed);
                bottomTr.eulerAngles = new Vector3(0, -45, 0);
            }
            if (MovingDirection == "SE")
            {
                rb.velocity = new Vector3(speed, 0, -speed);
                bottomTr.eulerAngles = new Vector3(0, 135, 0);
            }
            if (MovingDirection == "SW")
            {
                rb.velocity = new Vector3(-speed, 0, -speed);
                bottomTr.eulerAngles = new Vector3(0, 225, 0);
            }
        }

        //rotation
        topTr.rotation = QFacingDirection;

    }
}
