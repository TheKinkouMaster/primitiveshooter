﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {
    public float damagePerRound;
    public float distanceTravelled;
    public float range_dontChange;  //it is set up from other script, basing on range of weapon

    void Update()
    {
        distanceTravelled += VectorLenght(GetComponent<Rigidbody>().velocity) * Time.deltaTime;
        if (distanceTravelled >= range_dontChange) Destroy(gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<CharacterStats>() != null)
        {
            if (collision.gameObject.GetComponent<CharacterStats>().damageAllowed == true)
                collision.gameObject.GetComponent<CharacterStats>().currentHP = collision.gameObject.GetComponent<CharacterStats>().currentHP - damagePerRound;
        }
        else if (collision.gameObject.GetComponentInParent<CharacterStats>() != null)
        {
            if (collision.gameObject.GetComponentInParent<CharacterStats>().damageAllowed == true)
                collision.gameObject.GetComponentInParent<CharacterStats>().currentHP = collision.gameObject.GetComponentInParent<CharacterStats>().currentHP - damagePerRound;
        }

        Destroy(gameObject);
    }

    float VectorLenght(Vector3 vector)
    {
        float result = 0;

        result = Mathf.Sqrt(Mathf.Pow(vector.x,2) + Mathf.Pow(vector.y,2) + Mathf.Pow(vector.z,2));

        return result;
    }
}
