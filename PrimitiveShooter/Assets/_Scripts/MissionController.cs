﻿using UnityEngine;
using System.Collections;

public class MissionController : MonoBehaviour {
    public GameObject playerBanditPosition; //Possible positions to spawn player
    public GameObject playerUSPosition;

    public GameObject playerPrefab;         

    public GameObject Camera;

    public GameObject player;

    void Awake()
    {
        SetupPlayer();
    }


	void SetupPlayer()
    {
        //Picking spawn position
        Vector3 positionToSpawn = new Vector3(0, 0, 0);
        if (ShootingPractice_Options.playerFaction == "Bandits") positionToSpawn = playerBanditPosition.transform.position;
        else positionToSpawn = playerUSPosition.transform.position;

        //Creating Player
        player = (GameObject)Instantiate(playerPrefab, positionToSpawn, transform.rotation);

        //Changing Faction
        player.transform.Find("Faction").tag = ShootingPractice_Options.playerFaction;

        //Applying Difficulty
        int health = 100;
        if (ShootingPractice_Options.difficulty == "Easy") health = 200;
        else if (ShootingPractice_Options.difficulty == "Medium") health = 100;
        else if (ShootingPractice_Options.difficulty == "Hard") health = 50;
        player.GetComponent<CharacterStats>().maxHP = health;
        player.GetComponent<CharacterStats>().currentHP = health;

        //Attaching Camera to player
        Camera.GetComponent<CameraController>().objectAttached = player;

        //Setting up controlls
        GetComponent<PlayerController>().controlledObject = player;
        GetComponent<PlayerController>().GetObject();

        //Equipping weapons
        GetComponent<PlayerController>().SetWeapons(ShootingPractice_Options.playerPrimWeapPrefab, ShootingPractice_Options.playerSecWeapPrefab);
    }
}
