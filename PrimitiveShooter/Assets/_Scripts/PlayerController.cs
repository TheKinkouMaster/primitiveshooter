﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    public GameObject controlledObject = null; // When game starts script is being attached to an objects considered now as Player
    public GameObject firstWeap        = null; // Weapon references from main manu options
    public GameObject secWeap          = null; // 
    public char currentEquipped;               // Just a letter p/s to let program know which weapon is selected


    public string forwardKey  = "w";           //movement keys. Can be modified via editor. 
    public string backwardKey = "s";
    public string goLeftKey   = "a";
    public string goRightKey  = "d";
    public string SprintKey = "left shift";

    public GunController gunControllerReference = null; 

    

    public CharacterController scriptReference = null;


    
	
	
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) UnityEngine.SceneManagement.SceneManager.LoadScene("ShooterMenu"); 
        if (controlledObject != null)
        {
            //keyboard
            if (Input.GetKeyDown(KeyCode.Tab)) SwitchWeapon();
            if (Input.GetKey(backwardKey) && Input.GetKey(goRightKey)) scriptReference.MovingDirection = "SE";
            else if (Input.GetKey(backwardKey) && Input.GetKey(goLeftKey)) scriptReference.MovingDirection = "SW";
            else if (Input.GetKey(forwardKey) && Input.GetKey(goRightKey)) scriptReference.MovingDirection = "NE";
            else if (Input.GetKey(forwardKey) && Input.GetKey(goLeftKey)) scriptReference.MovingDirection = "NW";
            else if (Input.GetKey(backwardKey)) scriptReference.MovingDirection = "S";
            else if (Input.GetKey(forwardKey)) scriptReference.MovingDirection = "N";
            else if (Input.GetKey(goLeftKey)) scriptReference.MovingDirection = "W";
            else if (Input.GetKey(goRightKey)) scriptReference.MovingDirection = "E";
            else scriptReference.MovingDirection = null;


            if (Input.GetKey(SprintKey)) scriptReference.isSprinting = true;
            else scriptReference.isSprinting = false;

            //mouse
            Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(controlledObject.GetComponent<Transform>().position);
            Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);
            float angle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);
            scriptReference.QFacingDirection = Quaternion.Euler(new Vector3(0f, -angle - 90, 0f));

            if (Input.GetMouseButton(0)) gunControllerReference.isFiring = true;
            else gunControllerReference.isFiring = false;
        }
    }
    public void GetObject()
    {
        //if player is not set up at the beggining this function sets it up while game is playing
        scriptReference = controlledObject.GetComponent<CharacterController>();            //getting controlls
        gunControllerReference = controlledObject.GetComponentInChildren<GunController>(); //
        controlledObject.GetComponent<AIPropotype1_0>().enabled = false;                   //Disabling AI
        controlledObject.tag = "Player";
    }
    public void SetWeapons(GameObject primWeap, GameObject seconWeap)
    {
        firstWeap = primWeap;
        secWeap = seconWeap;
        currentEquipped = 's';
        SwitchWeapon();
    }
    void SwitchWeapon()
    {
        if (currentEquipped == 's')
        {
            controlledObject.GetComponentInChildren<GunController>().ChangeWeapon(firstWeap);
            currentEquipped = 'p';
        }
        else
        {
            controlledObject.GetComponentInChildren<GunController>().ChangeWeapon(secWeap);
            currentEquipped = 's';
        }

    }
    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }
}
