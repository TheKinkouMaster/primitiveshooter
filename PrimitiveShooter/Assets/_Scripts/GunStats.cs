﻿using UnityEngine;
using System.Collections;

public class GunStats : MonoBehaviour {
    //Informations about weapon
    public float initialBulletSpeed;
    public float rateOfFire;
    public float bulletSpread;
    public float range;
    public Transform BulletSpawnPoint;
    public GameObject BulletPrefab;
    public int cost;
}
