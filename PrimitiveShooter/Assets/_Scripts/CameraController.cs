﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
    public GameObject objectAttached = null;
    private bool objectIsAttached = false;

    public Vector3 cameraOffsetV3;
    public Vector3 cameraOffsetQ;

    void Update()
    {
        if (objectAttached == null && objectIsAttached == false)
        {
            objectAttached = GameObject.FindGameObjectWithTag("Player");
        }
        else
        {
            Quaternion position;

            transform.position = objectAttached.GetComponent<Transform>().position + cameraOffsetV3;
            position = objectAttached.GetComponent<Transform>().rotation;
            position.eulerAngles = cameraOffsetQ;

            transform.rotation = position;
        }
    }

}
