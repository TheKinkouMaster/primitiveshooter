﻿using UnityEngine;
using System.Collections;

public class CharacterStats : MonoBehaviour {
    public float speed;

    public float maxHP;
    public float currentHP;
    public bool damageAllowed;

    public int cost;

    void Update()
    {
        if (currentHP <= 0 && damageAllowed == true) Destroy(gameObject);
    }
}
