﻿using UnityEngine;
using System.Collections;

public class GunController : MonoBehaviour {
    public GameObject WeaponPrefab;       //Most of it is take from WeaponPrefab, so user can just drag and drop weapon, not all elements
    public GameObject BulletPrefab;
    public GunStats   WeaponPrefabStats;
    public GameObject weaponLocation;

    public bool isFiring = false;         //Changed via AI/Player
    
    public float delay;
    public float counter;
    public float initialBulletSpeed;
    public Vector3 gunPosition;

    private GameObject BulletSpawn;      
    public GameObject WeaponAttached;

    void Start()
    {
        if (WeaponAttached == null)
        {
            WeaponPrefabStats = WeaponPrefab.GetComponent<GunStats>();
            BulletPrefab = WeaponPrefab.GetComponent<GunStats>().BulletPrefab;

            delay = WeaponPrefabStats.rateOfFire;
            BulletPrefab = WeaponPrefabStats.BulletPrefab;
            initialBulletSpeed = WeaponPrefabStats.initialBulletSpeed;

            counter = 0;


            gunPosition = weaponLocation.transform.position;
            WeaponAttached = (GameObject)Instantiate(WeaponPrefab, gunPosition, GetComponent<Transform>().rotation);
            WeaponAttached.transform.parent = transform;
        }
    }

    public void ChangeWeapon(GameObject newWeap)
    {
        Destroy(WeaponAttached);
        WeaponPrefabStats = newWeap.GetComponent<GunStats>();
        BulletPrefab = newWeap.GetComponent<GunStats>().BulletPrefab;

        delay = WeaponPrefabStats.rateOfFire;
        BulletPrefab = WeaponPrefabStats.BulletPrefab;
        initialBulletSpeed = WeaponPrefabStats.initialBulletSpeed;

        counter = 0;


        gunPosition = weaponLocation.transform.position;
        WeaponAttached = (GameObject)Instantiate(newWeap, gunPosition, GetComponent<Transform>().rotation);
        WeaponAttached.transform.parent = transform;
    }

    void FixedUpdate()
    {
        counter = counter + Time.deltaTime;
        if (counter >= delay)
        {
            if (isFiring == true)
            {
                BulletSpawn = (GameObject)Instantiate(BulletPrefab, WeaponAttached.GetComponent<GunStats>().BulletSpawnPoint.position , SpreadSimulation(gameObject.GetComponent<Transform>().rotation, WeaponPrefabStats.bulletSpread));
                counter = 0;
                BulletSpawn.GetComponent<BulletScript>().range_dontChange = WeaponAttached.GetComponent<GunStats>().range;
                BulletSpawn.GetComponent<Rigidbody>().velocity = BulletSpawn.GetComponentInParent<Transform>().forward * initialBulletSpeed;
                BulletSpawn = null;
            }
            else counter = delay;
        }

        
    }

    private Quaternion spreadQ;
    private Quaternion result;
    Quaternion SpreadSimulation(Quaternion baseQuaternion, float spread)
    {
        spreadQ.Set((Random.value-0.5f) * spread, (Random.value-0.5f) * spread, (Random.value-0.5f) * spread, 0f);

        result.Set(baseQuaternion.x + spreadQ.x, baseQuaternion.y + spreadQ.y, baseQuaternion.z + spreadQ.z, baseQuaternion.w);

        return result;
    }
}
