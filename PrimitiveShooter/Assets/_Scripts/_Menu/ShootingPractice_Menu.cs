﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShootingPractice_Menu : MonoBehaviour {
    public GameObject mainMenuUI;
    public GameObject optionsUI;
    public GameObject missionUI;

    public Dropdown primWeap;
    public Dropdown secWeap;

    public GameObject[] weaponList;


    void Start()
    {
        EnableOptions();

        primWeap.options.Clear();
        secWeap.options.Clear();

        foreach (GameObject weap in weaponList)
        {
            primWeap.options.Add(new Dropdown.OptionData(weap.name));
            secWeap.options.Add(new Dropdown.OptionData(weap.name));
        }
        int TempInt = primWeap.value;
        primWeap.value = primWeap.value + 1;
        primWeap.value = TempInt;
        TempInt = secWeap.value;
        secWeap.value = secWeap.value + 1;
        secWeap.value = TempInt;


        EnableMainMenu();
    }

    public void EnableMainMenu()
    {
        mainMenuUI.SetActive(true);
        optionsUI.SetActive(false);
        missionUI.SetActive(false);
    }
    public void EnableOptions()
    {
        mainMenuUI.SetActive(false);
        optionsUI.SetActive(true);
        missionUI.SetActive(false);
    }
    public void EnableMission()
    {
        mainMenuUI.SetActive(false);
        optionsUI.SetActive(false);
        missionUI.SetActive(true);
    }

	public void Exit()
    {
        Destroy(GetComponent<ShootingPractice_Options>());
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }

    public void CallMission(string name)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(name);
    }
}
