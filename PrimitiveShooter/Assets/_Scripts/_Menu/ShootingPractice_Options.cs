﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShootingPractice_Options : MonoBehaviour {
    public static GameObject playerPrimWeapPrefab;
    public static GameObject playerSecWeapPrefab;
    public static string playerFaction;
    public static string difficulty;

    public Dropdown primWeap;
    public Dropdown secWeap;
    public Dropdown diff;
    public Dropdown faction;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void UpdateInfo()
    {
        playerFaction = faction.captionText.text;
        difficulty = diff.captionText.text;

        GameObject[] weaponList = GetComponent<ShootingPractice_Menu>().weaponList;

        for(int i = 0; i < weaponList.Length; i++)
        {
            if (weaponList[i].name == primWeap.captionText.text) playerPrimWeapPrefab = weaponList[i];
            if (weaponList[i].name == secWeap.captionText.text) playerSecWeapPrefab = weaponList[i];
        }
    }

    public void ChangePrimWeapon(string name)
    {
        playerPrimWeapPrefab = (GameObject)Resources.Load(name);
    }
}
