﻿using UnityEngine;
using System.Collections;

public class AIPropotype1_0 : MonoBehaviour {
    public GameObject waypoint;

    private Vector3 myPos;
    private Vector3 waypointPos;
    private Vector3 MovingDirection;

    public CharacterController controlledBottom;
    public GunController gunControllerReference;

    private float x = 0;
    private float z = 0;

    public float margin = 2;
    public float waitingTime = 0;
    public float waitingTimer = 0;

    public string enemyFaction;
    public string alliedFaction;
    private Vector3 enemyPosition;
    private Vector3 Myv3;
    

    public GameObject[] potentialTargetsArray;

    void Start()
    {
        controlledBottom = GetComponent<CharacterController>();
        gunControllerReference = GetComponentInChildren<GunController>();
    }


    void Update()
    {
        controlledBottom.MovingDirection = "STOP";
        if (waypoint != null)
        {
            myPos = GetComponent<Transform>().position;
            waypointPos = waypoint.GetComponent<Transform>().position;

            MovingDirection = waypointPos - myPos;

            x = MovingDirection.x;
            z = MovingDirection.z;

            if (x <= margin && x >= -margin && z <= margin && z >= -margin)
            {
                if (waitingTimer >= waitingTime)
                {
                    waypoint = waypoint.GetComponent<WaypointScript>().nextWaypoint;
                    waitingTimer = 0;
                }
                else waitingTimer = waitingTimer + Time.deltaTime;
            }
            else if (x >= 0 && z <= margin && z >= -margin) controlledBottom.MovingDirection = "E";
            else if (x <= 0 && z <= margin && z >= -margin) controlledBottom.MovingDirection = "W";
            else if (z >= 0 && x <= margin && x >= -margin) controlledBottom.MovingDirection = "N";
            else if (z <= 0 && x <= margin && x >= -margin) controlledBottom.MovingDirection = "S";
            else if (x >= 0 && z >= 0) controlledBottom.MovingDirection = "NE";
            else if (x <= 0 && z >= 0) controlledBottom.MovingDirection = "NW";
            else if (x >= 0 && z <= 0) controlledBottom.MovingDirection = "SE";
            else if (x <= 0 && z <= 0) controlledBottom.MovingDirection = "SW";
        }




        ////////////////////////////////////////////////////Shooting/Targeting/////////////////////////////////////////////////////////

        potentialTargetsArray = GameObject.FindGameObjectsWithTag(enemyFaction);
        enemyPosition.Set(0, 0, 0);
        foreach (GameObject enemy in potentialTargetsArray)
        {
            if (enemyPosition.x == 0 && enemyPosition.z == 0) enemyPosition = enemy.GetComponent<Transform>().position;
            if (vectorLenght(GetComponent<Transform>().position - enemyPosition) >= vectorLenght(GetComponent<Transform>().position - enemy.GetComponent<Transform>().position)) enemyPosition = enemy.GetComponent<Transform>().position;
        }

        ///////////Target Picked/////////
        if (enemyPosition.x != 0 && enemyPosition.y != 0 && enemyPosition.z != 0 && vectorLenght(GetComponent<Transform>().position - enemyPosition) <= gunControllerReference.WeaponAttached.GetComponent<GunStats>().range)
        {
            Vector3 positionOnScreen = GetComponent<Transform>().position;
            Vector3 mouseOnScreen = enemyPosition;
            float angle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);
            GetComponent<CharacterController>().QFacingDirection = Quaternion.Euler(new Vector3(0f, -angle - 90, 0f));
            gunControllerReference.isFiring = true;
        }
        else gunControllerReference.isFiring = false;

        //////////////allies presence correction//////////
        
        RaycastHit[] hits;
        Myv3.Set(enemyPosition.x- gunControllerReference.WeaponAttached.GetComponent<GunStats>().BulletSpawnPoint.position.x, 0, enemyPosition.z- gunControllerReference.WeaponAttached.GetComponent<GunStats>().BulletSpawnPoint.position.z);
        hits = Physics.RaycastAll(gunControllerReference.WeaponAttached.GetComponent<GunStats>().BulletSpawnPoint.position, Myv3, gunControllerReference.WeaponAttached.GetComponent<GunStats>().range);
        for (int i = 0; i < hits.Length; i++)
        {
            RaycastHit hit = hits[i];

            if (hit.transform.tag == "Character")
            {
                if (hit.transform.Find("Faction").tag == alliedFaction)
                {
                    gunControllerReference.isFiring = false;
                    break;
                }
            }
        }
    }

    

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.z - b.z, a.x - b.x) * Mathf.Rad2Deg;
    }
    float vectorLenght(Vector3 a)
    {
        float x1, z1;


        float lenght1;


        x1 = a.x;
        z1 = a.z;

        lenght1 = Mathf.Sqrt(Mathf.Pow(x1, 2) + Mathf.Pow(z1, 2));


        return lenght1;
    }

}
